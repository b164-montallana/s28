//Javascript Synchronous vs Asynchronous

//Synchronous Javascript
//
// console.log('Hello World');

// console.log('GoodBye');

// for(let i = 0; i <= 100; i++) {
//     console.log(i);
// }

// console.log('Hello World');

//Asynchronous Javascript

// function printMe() {
//     console.log('print me');
// }

// function test() {
//     console.log('test');
// }

// setTimeout(printMe, 5000);
// test();

//The fetch API allows you asynchronously request for a resource (data)
//the fetch recieves a PROMISE
//A promise is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.

//fetch() is a method in Javascript, w/c allow us to send a request to an API and process its response.
//syntax
    // fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})
//url => the url to resource/routes from the server.
//optional objects => it contains additional information about our request such as method, the body and the headers.

//it parse the response as JSON
//process the results

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));
// a promise 3 states: fulfilled, rejected, pending


//retrieves all post (GET)

fetch('https://jsonplaceholder.typicode.com/posts',{
    method: 'GET'
})
.then(response => response.json())
.then(data => {console.log(data)})

//"async" and "await" keywords is another approach that can be used to achieve asynchronous code
//used in functions to indicate which portions of code should be waited for 

async function fetchData(){

    let result = await fetch('https://jsonplaceholder.typicode.com/posts')

    //result returned by fetch
    console.log(result);

    console.log(typeof result);

    console.log(result.body);

    //converts data from the response object as JSON
    let json = await result.json();

    console.log(json);

}

fetchData()


//Creating a post
fetch('https://jsonplaceholder.typicode.com/posts',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'New post',
        body: 'Hello World',
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


//updating a post
//updating a post following the rest API (update, PUT)

fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        id: 1,
        title: "Updated Post",
        body: "Hello Again",
        userId: 1
    })
})
.then(res => res.json())
.then(data => console.log(data))

//Patch method alternative to PUT
fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        id: 1,
        title: "Updated Post",
       
    })
})
.then(res => res.json())
.then(data => console.log(data))

//Delete a Post
fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))


//Filtering posts
//The data can be filtered by sending the userId along with the URL
//?
/* 
    Syntax:
    Individual parameter 
        'url?parameterName=value'
*/
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))

//Multiple parameter
/* 
    Syntax:
    Individual parameter 
        'url?parameterNameA=value'
*/
fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2')
.then(res => res.json())
.then(data => console.log(data))

//Retrieving nested/related data
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))
