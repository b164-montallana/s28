//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos',{
    method: 'GET'
})
.then(response => response.json())
.then(data => {console.log(data)})

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
const api_url = 'https://jsonplaceholder.typicode.com/todos';
async function getTitle() {
    const response = await fetch(api_url);
    const data = await response.json();
    const titles = data.map(data => data.title);
    console.log(titles);
    
}

getTitle()

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'GET',
})
.then(response => response.json())
.then(data => {console.log(data)})

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
async function getData() {
    const response = await fetch(api_url);
    const data = await response.json();
    const title = data.map(data => data.title);
    const status = data.map(data => data.completed);
    let result = {
        title: title,
        status: status
    }
    console.log(result); 
}

getData()

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        id: 201,
        title: 'Created to do list item',
        completed: false,
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
/* 
    9. Update a to do list item by changing the data structure to contain the following properties:
    - Title
    - Description
    - Status
    - Date Completed
    - User ID
*/
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with different data structure",
        title: 'Updated to do List Item',
        status: "Pending",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "03/30/22",
        status: "Complete",
        
    })
})
.then(response => response.json())
.then(json => console.log(json))

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))